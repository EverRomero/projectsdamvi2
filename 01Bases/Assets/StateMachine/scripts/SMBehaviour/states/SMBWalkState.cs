using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace m17
{
    public class SMBWalkState : MBState
    {
        private PJSMB m_PJ;
        private Rigidbody2D m_Rigidbody;
        private Animator m_Animator;
        private FiniteStateMachine m_StateMachine;

        private Vector2 m_Movement;

        [SerializeField]
        private float m_Speed = 3;

        private void Awake()
        {
            m_PJ = GetComponent<PJSMB>();
            m_Rigidbody = GetComponent<Rigidbody2D>();
            m_Animator = GetComponent<Animator>();
            m_StateMachine = GetComponent<FiniteStateMachine>();
        }

        public override void Init()
        {
            base.Init();
            m_PJ.Input.FindActionMap("Standard").FindAction("Attack").performed += OnAttack;
            m_Animator.Play("walk");
        }

        public override void Exit()
        {
            base.Exit();
            m_PJ.Input.FindActionMap("Standard").FindAction("Attack").performed -= OnAttack;
        }

        private void OnAttack(InputAction.CallbackContext context)
        {
            m_StateMachine.ChangeState<SMBHit1State>();
        }

        private void Update()
        {
            m_Movement = m_PJ.MovementAction.ReadValue<Vector2>();

            if(m_Movement ==  Vector2.zero)
                m_StateMachine.ChangeState<SMBIdleState>();
        }

        private void FixedUpdate()
        {
            m_Rigidbody.velocity = Vector2.right * m_Movement * m_Speed;
        }
    }
}

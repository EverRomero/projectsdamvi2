using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(SpriteRenderer))]
public class InputActionClick : MonoBehaviour
{
    [SerializeField]
    private InputActionAsset m_InputAsset;
    private InputActionAsset m_Input;
    private InputAction m_PointerPosition;

    // Start is called before the first frame update

    void Awake()
    {

        m_Input = Instantiate(m_InputAsset);

        m_Input.FindActionMap("Default").FindAction("PlayerClick").performed += ButtonAction;
        m_PointerPosition = m_Input.FindActionMap("Default").FindAction("PlayerPosition");

        m_Input.FindActionMap("Default").Enable();
    }

    private void OnDestroy()
    {
        m_Input.FindActionMap("Default").FindAction("PlayerClick").performed -= ButtonAction;
    }

    private void ButtonAction(InputAction.CallbackContext context)
    {

        Vector2 pointerPosition = m_PointerPosition.ReadValue<Vector2>();

        //Raig que parteix de la c�mera i va cap a l'escena
        Ray cameraRay = Camera.main.ScreenPointToRay(pointerPosition);

        //Efectuem el raycast i apliquem filtre de layers
        RaycastHit2D hit = Physics2D.Raycast(cameraRay.origin, cameraRay.direction, Mathf.Infinity);

        //Nom�s en cas d'impactar quelcom
        if (hit.rigidbody != null)
        {
            Debug.Log(string.Format("He tocado {0}", hit.rigidbody.name));

            //S� que nom�s s�n enemics perqu� he filtrat per layer
            BlackNoteController blackNote = hit.rigidbody.GetComponent<BlackNoteController>();

            //Per� igualment podem fer la comprovaci�
            if (blackNote != null)
                blackNote.OnRaycasted();
        }
        else
        {
            Debug.Log("No toque nada");
            GameManager.Instance.ChangeScene("LoseScene");
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    private static GameManager m_Instance;
    public static GameManager Instance => m_Instance;

        
    private float m_Speed = 3;  
    public float m_GetSetSpeed
    {

        get { return m_Speed; }
        set { m_Speed = value; }

    }

    [SerializeField]
    private Points m_TopScore;
    public int GetSetTopScore
    {
        get
        {
            return m_TopScore.puntos;
        }
        set
        {
            if (value > m_TopScore.puntos)
                m_TopScore.puntos = value;
        }
    }


    private int m_Score = 0;
    public int m_GetSetScore
    {

        get { return m_Score; }
        set { m_Score = value; }

    }

    private void Awake() 
    {

        if (m_Instance == null)
        {
            m_Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }


        DontDestroyOnLoad(gameObject);

        InitValues();  
    }


    public void ChangeScene(string scene)
    {
        if(scene == "LoseScene")
        {
           GameManager.Instance.GetSetTopScore = GameManager.Instance.m_GetSetScore;
        }
        SceneManager.LoadScene(scene);
    }

    public void InitValues()
    {
        m_Score = 0;
        m_Speed = 3;
    }  

    public void plus()
    {
        this.m_GetSetScore++;
    }

}


    


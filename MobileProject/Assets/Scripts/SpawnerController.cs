using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class SpawnerController : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    Transform[] m_PosBlackNote;

    [SerializeField]
    AudioClip[] m_Sounds;
    void Start()
    {

        StartCoroutine(m_CreateBlackNote());

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator m_CreateBlackNote()
    {

        while (true)
        {
       
           for (int i = 0; i < this.transform.childCount; i++) {

                Debug.Log("Funcionoxd");

                int RandomIndex = Random.Range(0, m_PosBlackNote.Length);


                GameObject m_newBlackNote = this.transform.GetChild(i).gameObject;

                if (!m_newBlackNote.active)
                {
                    m_newBlackNote.SetActive(true);
                    m_newBlackNote.transform.position = m_PosBlackNote[RandomIndex].position;
                    m_newBlackNote.GetComponent<BlackNoteController>().m_GetSetSound = m_Sounds[RandomIndex];
                    m_newBlackNote.GetComponent<BlackNoteController>().Init();
                }

                float m_waitTime = Random.Range(1f, 1.5f);

                yield return new WaitForSeconds(m_waitTime);
            }

        }

        

    }

}

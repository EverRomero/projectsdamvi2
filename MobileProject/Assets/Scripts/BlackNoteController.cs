using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class BlackNoteController : MonoBehaviour
{



    [SerializeField]
    private GameEvent m_Muerto;

    [SerializeField]
    private GameObject m_Explosion;

    private AudioClip sound;
    public AudioClip m_GetSetSound
    {

        get {return sound; }
        set{ sound = value; }

    }

    void Start()
    {
        Init();
    }

    public void Init()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector3(0, -(GameManager.Instance.m_GetSetSpeed));
    }

    public void OnRaycasted()
    {
        Debug.Log(string.Format("I'VE BEEN RAYCASTED! {0}", name));


        blackNoteDestroy();
       

        
    }

    public void blackNoteDestroy()
    {

        Instantiate(m_Explosion, transform.position, Quaternion.identity);
        AudioSource.PlayClipAtPoint(sound, Camera.main.transform.position);
        this.gameObject.SetActive(false);
        dificultUp();
        m_Muerto.Raise();
    }

    private void dificultUp()
    {


        if (GameManager.Instance.m_GetSetScore % 10 == 0 && GameManager.Instance.m_GetSetScore != 0)
        {
           GameManager.Instance.m_GetSetSpeed = GameManager.Instance.m_GetSetSpeed + 2.0f;
        }
      

        Debug.Log("Speed: " + GameManager.Instance.m_GetSetSpeed);

    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New Points", menuName = "Points")]
public class Points : ScriptableObject
{

    public int puntos;
}

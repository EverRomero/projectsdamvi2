using System.Collections;
using System.Collections.Generic;
using UnityEditor.SearchService;
using UnityEngine;
using UnityEngine.SceneManagement;
using static UnityEngine.EventSystems.EventTrigger;

public class GameManager : MonoBehaviour
{

    private static GameManager m_Instance;
    public static GameManager Instance => m_Instance;

    public List<Transform> GameManagerWayPoints;

    public List<Transform> GameManagerSpawnPoints;

    public GameObject gameManagerPlayer;

    [SerializeField]
    public SpawnController spawnController;

    private int wave = 1;
    public int GetSetWave
    {

        get { return wave; }
        set { wave = value; }

    }

    private int waveEnemies = 0;
    public int GetSetwaveEnemies
    {

        get { return waveEnemies; }
        set { waveEnemies = value; }
    }

    private void Awake()
    {

        if (m_Instance == null)
        {
            m_Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }


        DontDestroyOnLoad(gameObject);

    }

    public void InitValues()
    {

       GetSetWave = 1;
     

    }

    private void Start()
    {
        
    }

    public void ChangeScene(string scene)
    {
  
        SceneManager.LoadScene(scene);

    }

    public void ChangeSceneGame()
    {

        SceneManager.LoadScene("GameScene");
        InitValues();
        spawnController.runWave();
    }

    public void waveAug()
    {

        wave++;
    }

    public void enemiesMinor()
    {

        waveEnemies--;
    }


    public void setPlayer(GameObject player)
    {
        gameManagerPlayer = player;
    }

    public void DestroyPlayer(GameObject player)
    {
        Destroy(gameManagerPlayer);
    }

}

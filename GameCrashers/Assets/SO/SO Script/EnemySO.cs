using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[CreateAssetMenu(fileName = "Enemy", menuName = "Enemys SO")]
public class EnemySO : ScriptableObject
{

   public int Damage;
   public int Healt;
   public float Speed;
   public Color SpriteColor;
   public float DetectionArea;
   

}

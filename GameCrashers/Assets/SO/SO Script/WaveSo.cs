using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Wave", menuName = "Waves SO")]
public class WaveSo : ScriptableObject
{
    public GameObject MeleeEnemy;
    public GameObject RangeEnemy;
    public GameObject MeleeEnemy2;
    public GameObject RangeEnemy2;

    public int numberMeleeEnemy;
    public int numberRangeEnemy;

    public void RandomWave()
    {
        numberMeleeEnemy = Random.Range(1, 6);

        numberRangeEnemy = Random.Range(1, 6);

    }
}

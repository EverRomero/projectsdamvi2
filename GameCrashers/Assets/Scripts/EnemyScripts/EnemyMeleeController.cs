using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class EnemyController : MonoBehaviour
{

    [SerializeField]
    private GameObject player;

    [SerializeField]
    private EnemySO enemySO;

    [SerializeField]
    private GameEvent gameEvent;

    [SerializeField]
    private GameEvent gameEvent2;

    [SerializeField]
    private MeleeEnemyDetectionArea detectionArea;

    [SerializeField]
    private MeleeEnemyAttackArea attackArea;

    [SerializeField]
    private HitboxInfo hitboxInfo;

    [SerializeField]
    private SpriteRenderer meleeSpriteRenderer;

    [SerializeField]
    private int hitDamage = 2;

    [SerializeField]
    float moveEnemySpeed = 3f;

    [SerializeField]
    private int maxHp = 10;

    [SerializeField]
    private int currentHp = 0;

    private Rigidbody2D meleeEnemyRigidBody;
    private Animator meleeEnemyAnimator;

    //private bool meleeEnemyPlayerEnter = false;


    private void Awake()
    {

        hitDamage = enemySO.Damage;
        moveEnemySpeed = enemySO.Speed;
        maxHp = enemySO.Healt;
        currentHp = maxHp;
        meleeSpriteRenderer.color = enemySO.SpriteColor;
        detectionArea.gameObject.transform.localScale = new Vector3(enemySO.DetectionArea, enemySO.DetectionArea, 1);


        

        meleeEnemyAnimator = GetComponent<Animator>();
        meleeEnemyRigidBody = GetComponent<Rigidbody2D>();
    }

    // Start is called before the first frame update
    void Start()
    {
        player = GameManager.Instance.gameManagerPlayer;
        InitState(SwitchMachineStates.IDLE);
    }

    // Update is called once per frame
    void Update()
    {

        UpdateState();
    }

    public void Chase()
    {
        Assert.IsNotNull(player);

        Vector2 newPosition = this.transform.position + (player.transform.position - this.transform.position).normalized * moveEnemySpeed * Time.fixedDeltaTime;

        if (this.transform.position.x < newPosition.x)
        {
            this.gameObject.transform.eulerAngles = new Vector3(0, 0, 0);

        }
        else if (this.transform.position.x > newPosition.x)
        {

            this.gameObject.transform.eulerAngles = new Vector3(0, 180, 0);
        }

        meleeEnemyRigidBody.MovePosition(newPosition);
        
    }

    public void StopChase()
    {

        this.transform.position = this.transform.position;

    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {

            meleeEnemyRigidBody.bodyType = RigidbodyType2D.Static;


        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "HitBox")
        {
            Debug.Log(string.Format("I've been hit by {0} and it did {1} damage to me",
               collision.name,
               collision.gameObject.GetComponent<HitboxInfo>().Damage));

            this.currentHp = this.currentHp - collision.gameObject.GetComponent<HitboxInfo>().Damage;

            if(this.currentHp <= 0)
            {
                GameManager.Instance.enemiesMinor();
                gameEvent2.Raise();

                if (GameManager.Instance.GetSetwaveEnemies == 0)
                {
                    GameManager.Instance.waveAug();
                    gameEvent.Raise();

                }
                Destroy(this.gameObject);
            }

        }

    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {

            meleeEnemyRigidBody.bodyType = RigidbodyType2D.Dynamic;
        }

        
    }

    // --------------------------------------------------   STATE MACHINE -------------------------------------------------------------//

    private enum SwitchMachineStates { NONE, IDLE, WALK, HIT1};
    private SwitchMachineStates m_CurrentState;

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                meleeEnemyAnimator.Play("IdleMeleeEnemy");

                break;

            case SwitchMachineStates.WALK:

                meleeEnemyAnimator.Play("MovementMeleeEnemy");

                break;

            case SwitchMachineStates.HIT1:

                meleeEnemyRigidBody.velocity = Vector2.zero;
                hitboxInfo.Damage = hitDamage;
                meleeEnemyAnimator.Play("AttackMeleeEnemy");
                break;

            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.HIT1:

                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                StopChase();
                if (detectionArea.playerEnter)
                {
                    ChangeState(SwitchMachineStates.WALK);

                }


                break;
            case SwitchMachineStates.WALK:
                Chase();
                if (!detectionArea.playerEnter)
                {
                    ChangeState(SwitchMachineStates.IDLE);

                }
                if (attackArea.attackPlayerEnter)
                {
                    ChangeState(SwitchMachineStates.HIT1);
                }


                break;
            case SwitchMachineStates.HIT1:
                if (!attackArea.attackPlayerEnter)
                {
                    EndHit();
                }
                break;

            default:
                break;
        } 
    }


    public void EndHit()
    {
        ChangeState(SwitchMachineStates.IDLE);
    }

    private void OnDestroy()
    {

        StopChase();
    }

}


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeEnemyAttackArea : MonoBehaviour
{

    public bool attackPlayerEnter = false;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Player")
        {
            attackPlayerEnter = true;

        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Player")
        {

            attackPlayerEnter = false;

        }

    }
}

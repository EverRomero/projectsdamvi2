using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Assertions;

public class EnemyRangerController : MonoBehaviour
{
    [SerializeField]
    private GameObject player;

    [SerializeField]
    private EnemySO enemySO;

    [SerializeField]
    private GameEvent gameEvent;


    [SerializeField]
    private GameEvent gameEvent2;

    [SerializeField]
    private RangeEnemyDetectionArea detectionArea;

    [SerializeField]
    private RangeEnemyAttackArea attackArea;

    [SerializeField]
    private List<Transform> WayPoints;

    [SerializeField]
    private SpriteRenderer rangeSpriteRenderer;

    private int index = 0;

    [SerializeField]
    private HitboxInfo hitboxInfo;

    [SerializeField]
    private int hitDamage = 2;

    [SerializeField]
    float moveEnemySpeed = 6f;

    [SerializeField]
    private int maxHp = 10;

    [SerializeField]
    private int currentHp = 0;

    private Rigidbody2D rangeEnemyRigidBody;
    private Animator rangeEnemyAnimator;

    //private bool meleeEnemyPlayerEnter = false;


    private void Awake()
    {

        hitDamage = enemySO.Damage;
        moveEnemySpeed = enemySO.Speed;
        maxHp = enemySO.Healt;
        currentHp = maxHp;
        rangeSpriteRenderer.color = enemySO.SpriteColor;
        detectionArea.gameObject.transform.localScale = new Vector3(enemySO.DetectionArea, enemySO.DetectionArea, 1);


        rangeEnemyAnimator = GetComponent<Animator>();
        rangeEnemyRigidBody = GetComponent<Rigidbody2D>();
    }

    // Start is called before the first frame update
    void Start()
    {

        player = GameManager.Instance.gameManagerPlayer;
        WayPoints = GameManager.Instance.GameManagerWayPoints;
      

        InitState(SwitchMachineStates.PATROL);
    }

    // Update is called once per frame
    void Update()
    {

        UpdateState();
    }

    public void Patrol()
    {

        if(WayPoints != null)
        {


            Vector3 destination = WayPoints[0].position;

            switch (index)
            {

                case 0:
                    destination = WayPoints[0].position;
                    break;
                case 1:
                    destination = WayPoints[1].position;
                    break;
                case 2:
                    destination = WayPoints[2].position;
                    break;
                case 3:
                    destination = WayPoints[0].position;
                    break;
                case 4:
                    destination = WayPoints[3].position;
                    break;
                case 5:
                    destination = WayPoints[4].position;
                    break;
                case 6:
                    destination = WayPoints[3].position;
                    break;

            }


            Vector2 newPosition = this.transform.position + (destination - this.transform.position).normalized * moveEnemySpeed * Time.fixedDeltaTime;



            if (this.transform.position.x < newPosition.x)
            {
                this.gameObject.transform.eulerAngles = new Vector3(0, 0, 0);

            }
            else if (this.transform.position.x > newPosition.x)
            {

                this.gameObject.transform.eulerAngles = new Vector3(0, 180, 0);
            }

            rangeEnemyRigidBody.MovePosition(newPosition);

            float distance = Vector3.Distance(this.transform.position, destination);


            if (distance <= 0.5) {

                index++;
                if (index > 6)
                {

                    index = 0;
                }
            }

            }
    }

    public void Chase()
    {
  
        Vector2 newPosition = this.transform.position + (player.transform.position - this.transform.position).normalized * moveEnemySpeed * Time.fixedDeltaTime;

        if (this.transform.position.x < newPosition.x)
        {
            this.gameObject.transform.eulerAngles = new Vector3(0, 0, 0);

        }
        else if (this.transform.position.x > newPosition.x)
        {

            this.gameObject.transform.eulerAngles = new Vector3(0, 180, 0);
        }

        rangeEnemyRigidBody.MovePosition(newPosition);
        
    }

    public void StopChase()
    {

        this.transform.position = this.transform.position;

        if (this.isActiveAndEnabled)
        {
            StartCoroutine(timeWaitStopChase());

        }

  

       
    }

    public IEnumerator timeWaitStopChase()
    {

         yield return new WaitForSeconds(2);
        ChangeState(SwitchMachineStates.PATROL);
    }


    /*private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {

            meleeEnemyPlayerEnter = true;

        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {

            meleeEnemyPlayerEnter = false;

        }
    }*/

    private void OnTriggerEnter2D(Collider2D collision)
    {

        Assert.IsNotNull(player);

        if (collision.gameObject.tag == "HitBox")
        {
            Debug.Log(string.Format("I've been hit by {0} and it did {1} damage to me",
               collision.name,
               collision.gameObject.GetComponent<HitboxInfo>().Damage));

            this.currentHp = this.currentHp - collision.gameObject.GetComponent<HitboxInfo>().Damage;

            if (this.currentHp <= 0){ 

                GameManager.Instance.enemiesMinor();
                gameEvent2.Raise();
                if (GameManager.Instance.GetSetwaveEnemies == 0)
                {
                    GameManager.Instance.waveAug();
                    gameEvent.Raise();

                }
                Destroy(this.gameObject);
            }

        }
    }

    // --------------------------------------------------   STATE MACHINE -------------------------------------------------------------//

    private enum SwitchMachineStates { NONE, IDLE, WALK, PATROL, HIT1 };
    private SwitchMachineStates m_CurrentState;

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                rangeEnemyAnimator.Play("RangeEnemyIdle");

                break;

            case SwitchMachineStates.WALK:

                rangeEnemyAnimator.Play("RangeEnemyWalk");

                break;

            case SwitchMachineStates.PATROL:

                rangeEnemyAnimator.Play("RangeEnemyWalk");

                break;

            case SwitchMachineStates.HIT1:

                rangeEnemyRigidBody.velocity = Vector2.zero;
                hitboxInfo.Damage = hitDamage;
                rangeEnemyAnimator.Play("RangeEnemyAttack");
                break;

            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.HIT1:

                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                StopChase();
               
                break;

            case SwitchMachineStates.WALK:
                Chase();
                if (!detectionArea.playerEnter)
                {
                    ChangeState(SwitchMachineStates.IDLE);

                }
                if (attackArea.attackPlayerEnter)
                {
                    ChangeState(SwitchMachineStates.HIT1);
                }
                break;

            case SwitchMachineStates.PATROL:
                Patrol();
                if (detectionArea.playerEnter)
                {
                    ChangeState (SwitchMachineStates.WALK);
                }

                break;
            case SwitchMachineStates.HIT1:
                if (!attackArea.attackPlayerEnter)
                {
                    EndHit();
                }
                break;

            default:
                break;
        }
    }


    public void EndHit()
    {
        ChangeState(SwitchMachineStates.IDLE);
    }

    private void OnDestroy()
    {
        
        StopChase (); 
    }
}

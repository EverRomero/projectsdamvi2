using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    [SerializeField]
    GameObject Player;

    [SerializeField]
    Transform[] cameraLimits;

    private bool x1 = false;
    private bool x2 = false;
    private bool y1 = false;
    private bool y2 = false;

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (Player != null)
        {


            if (Player.transform.position.x < cameraLimits[0].position.x)
            {
                x1 = true;
            }
            else
            {
                x1 = false;
            }

            if (Player.transform.position.y > cameraLimits[2].position.y)
            {

                y1 = true;
            }
            else
            {
                y1 = false;
            }

            if (Player.transform.position.y < cameraLimits[3].position.y)
            {
                y2 = true;
            }
            else
            {
                y2 = false;
            }

            if (Player.transform.position.x > cameraLimits[1].position.x)
            {
                x2 = true;
            }
            else
            {
                x2 = false;
            }



            if (x1 && y1)
            {
                this.transform.position = new Vector3(cameraLimits[0].position.x, cameraLimits[2].position.y, -10);

            }
            else if (x1 && y2)
            {

                this.transform.position = new Vector3(cameraLimits[0].position.x, cameraLimits[3].position.y, -10);

            }
            else if (x2 && y1)
            {
                this.transform.position = new Vector3(cameraLimits[1].position.x, cameraLimits[2].position.y, -10);

            }
            else if (x2 && y2)
            {
                this.transform.position = new Vector3(cameraLimits[1].position.x, cameraLimits[3].position.y, -10);

            }
            else if (x1)
            {
                this.transform.position = new Vector3(cameraLimits[0].position.x, Player.transform.position.y, -10);

            }
            else if (y1)
            {
                this.transform.position = new Vector3(Player.transform.position.x, cameraLimits[2].position.y, -10);

            }
            else if (y2)
            {
                this.transform.position = new Vector3(Player.transform.position.x, cameraLimits[3].position.y, -10);

            }
            else if (x2)
            {
                this.transform.position = new Vector3(cameraLimits[1].position.x, Player.transform.position.y, -10);

            }
            else
            {

                this.transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y, -10);
            }


        }
    }
}

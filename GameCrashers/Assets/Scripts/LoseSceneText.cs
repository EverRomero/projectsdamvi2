using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LoseSceneText : MonoBehaviour
{
    public TMP_Text text;

    // Start is called before the first frame update
    void Start()
    {
        text.text = "you survived for "+GameManager.Instance.GetSetWave+" waves";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

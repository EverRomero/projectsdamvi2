using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WaveTextController : MonoBehaviour
{


    public TMP_Text text;

    public void updateTextWave()
    {

        text.text = "Wave: " + GameManager.Instance.GetSetWave;

    }

}

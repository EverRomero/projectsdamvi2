using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;
using UnityEngine.Windows;

public class PlayerController : MonoBehaviour
{

    [SerializeField]
    private int maxHealt = 20;

    private int currentHealt = 20;

    public HealtBar healtBar;


    private void Start()
    {

        currentHealt = maxHealt;
        healtBar.SetMaxHealt(maxHealt);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

  
        if (collision.gameObject.tag == "HitBox")
        {
            Debug.Log(string.Format("I've been hit by {0} and it did {1} damage to me",
               collision.name,
               collision.gameObject.GetComponent<HitboxInfo>().Damage));

            currentHealt = currentHealt - collision.gameObject.GetComponent<HitboxInfo>().Damage;

            healtBar.SetHealt(currentHealt);

            if(currentHealt <= 0)
            {
           
                GameManager.Instance.DestroyPlayer(gameObject);
                GameManager.Instance.ChangeScene("LoseScene");
            }

        }
    }

    

}

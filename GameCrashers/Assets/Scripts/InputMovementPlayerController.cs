using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.Timeline.Actions;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;
using UnityEngine.Windows;

public class InputMovementPlayerController : MonoBehaviour
{

    [SerializeField]
    private InputActionAsset inputAsset;
    private InputActionAsset input;
    private InputAction movementPlayerAction;
    private Animator playerAnimator;
    private Rigidbody2D playerRigidbody;


    [SerializeField]
    private HitboxInfo hitboxInfo;

    [Header("Character Values")]

    [SerializeField]
    private int hit1Damage = 2;

    [SerializeField]
    private int hit2Damage = 5;

    [SerializeField]
    private float playerMoveSpeed = 6f;


    private void Awake()
    {

        Assert.IsNotNull(inputAsset);

        input = Instantiate(inputAsset);
        movementPlayerAction = input.FindActionMap("Default").FindAction("MovementPlayerAction");
        input.FindActionMap("Default").FindAction("WeakAttack").performed  += WeakAttackAction;
        input.FindActionMap("Default").FindAction("StrongAttack").performed += StrongAttackAction;
        input.FindActionMap("Default").Enable();

        playerRigidbody = GetComponent <Rigidbody2D>(); 
        playerAnimator = GetComponent <Animator>();

    }

    private void Start()
    {
        GameManager.Instance.setPlayer(this.gameObject);
        InitState(SwitchMachineStates.IDLE);
    }

    void Update()
    {
            
        UpdateState();
  
    }

    


    // --------------------------------------------------   STATE MACHINE -------------------------------------------------------------//

    private enum SwitchMachineStates { NONE, IDLE, WALK, HIT1, HIT2 };
    private SwitchMachineStates m_CurrentState;

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                playerRigidbody.velocity = Vector2.zero;
                playerAnimator.Play("IdlePlayer");

                break;

            case SwitchMachineStates.WALK:

                playerAnimator.Play("MovementPlayer");

                break;

            case SwitchMachineStates.HIT1:

                comboAvailable = false;
                playerRigidbody.velocity = Vector2.zero;
                hitboxInfo.Damage = hit1Damage;
                playerAnimator.Play("WeakAttackPlayer");    
         
                break;

            case SwitchMachineStates.HIT2:

                comboAvailable = false;
                playerRigidbody.velocity = Vector2.zero;
                hitboxInfo.Damage = hit2Damage;
                playerAnimator.Play("StrongAttackPlayer");
          
                break;

            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.HIT1:

                comboAvailable = false;

                break;

            case SwitchMachineStates.HIT2:

                comboAvailable = false;

                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
      
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                if (movementPlayerAction.ReadValue<Vector2>().x != 0 || movementPlayerAction.ReadValue<Vector2>().y != 0)
                    ChangeState(SwitchMachineStates.WALK);

                break;
            case SwitchMachineStates.WALK:

                if (movementPlayerAction.ReadValue<Vector2>().x > 0)
                {

                    this.gameObject.transform.eulerAngles = new Vector3(0, 0, 0);

                }
                else if (movementPlayerAction.ReadValue<Vector2>().x < 0)
                {
                    this.gameObject.transform.eulerAngles = new Vector3(0, 180, 0);
                }

                playerRigidbody.velocity = movementPlayerAction.ReadValue<Vector2>() * playerMoveSpeed;

                if (playerRigidbody.velocity == Vector2.zero)
                    ChangeState(SwitchMachineStates.IDLE);

                break;
            case SwitchMachineStates.HIT1:
                break;

            case SwitchMachineStates.HIT2:
                break;

            default:
                break;
        }

    }

    private bool comboAvailable = false;

    public void InitComboWindow()
    {
        comboAvailable = true;
    }

    public void EndComboWindow()
    {
        comboAvailable = false;
    }

    public void EndHit()
    {
        ChangeState(SwitchMachineStates.IDLE);
    }
    

    public void WeakAttackAction(InputAction.CallbackContext actionContext)
    {
        switch (m_CurrentState)
        {

            case SwitchMachineStates.HIT1:

                if (comboAvailable)
                {
                    ChangeState(SwitchMachineStates.HIT2);
                }
                break;

            default:
                ChangeState(SwitchMachineStates.HIT1);
                break;
        }    
    }

    public void StrongAttackAction(InputAction.CallbackContext actionContext) {

        switch (m_CurrentState)
        {
         
            case SwitchMachineStates.HIT2:
                if (comboAvailable)
                {
                    ChangeState(SwitchMachineStates.HIT1);
                }
                break;

            default:
                ChangeState(SwitchMachineStates.HIT2);
                break;
        }
        

    }

    public void endAnimation()
    {

        EndHit();
    }

    private void OnDestroy()
    {

        input.FindActionMap("Default").FindAction("WeakAttack").performed -= WeakAttackAction;
        input.FindActionMap("Default").FindAction("StrongAttack").performed -= StrongAttackAction;
        input.FindActionMap("Default").Disable();

    }
}

using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Tilemaps;

public class SpawnController : MonoBehaviour
{

    private List<Transform> SpawnPoints;

    private int randomEnemy;
    private int randomSpawnPoint;
    private int randomSpawnPoint2;
    private GameObject enemyMelee;
    private GameObject enemyRange;

    [SerializeField]
    public GameEvent gameEvent;

    [SerializeField]
    private WaveSo wave1;

    [SerializeField]
    private WaveSo wave2;

    [SerializeField]
    private WaveSo randomWave;

    // Start is called before the first frame update
    void Start()
    {

        SpawnPoints = GameManager.Instance.GameManagerSpawnPoints;
        runWave();

    }

    public void Waves(int wave)
    {
        switch (wave)
        {

            case 1:

         
                Debug.Log(wave1.numberMeleeEnemy);

                SpawnPoints = GameManager.Instance.GameManagerSpawnPoints;
                GameManager.Instance.GetSetwaveEnemies = wave1.numberMeleeEnemy + wave1.numberRangeEnemy;

                gameEvent.Raise();

                for (int i = 0; i < wave1.numberMeleeEnemy; i++)
                {
                    Debug.Log("Estoy dentro");

                    Debug.Log(wave1.numberMeleeEnemy);

                    Debug.Log(SpawnPoints.Count);

                    Debug.Log(GameManager.Instance.GetSetwaveEnemies);

                    randomEnemy = Random.Range(0, 2);
                    randomSpawnPoint = Random.Range(0, 6);

                    if (randomEnemy == 0)
                    {
                        enemyMelee = Instantiate(wave1.MeleeEnemy);
                    }
                    else
                    {
                        enemyMelee = Instantiate(wave1.MeleeEnemy2);

                    }

                    enemyMelee.gameObject.transform.position = SpawnPoints[randomSpawnPoint].position;
                }

                break;

            case 2:

                GameManager.Instance.GetSetwaveEnemies = wave2.numberMeleeEnemy + wave2.numberRangeEnemy;
                SpawnPoints = GameManager.Instance.GameManagerSpawnPoints;
                gameEvent.Raise();

                for (int i = 0; i < wave2.numberRangeEnemy; i++)
                {
                    randomEnemy = Random.Range(0, 2);
                    randomSpawnPoint = Random.Range(0, 6);

                    if (randomEnemy == 0)
                    {
                        enemyRange = Instantiate(wave2.RangeEnemy);
                    }
                    else
                    {
                        enemyRange = Instantiate(wave2.RangeEnemy2);

                    }

                    if (enemyRange != null)
                    {
                        enemyRange.gameObject.transform.position = SpawnPoints[randomSpawnPoint].position;
                    }
                }
 
                break;

            default:

                randomWave.RandomWave();
                SpawnPoints = GameManager.Instance.GameManagerSpawnPoints;
                GameManager.Instance.GetSetwaveEnemies = randomWave.numberMeleeEnemy + randomWave.numberRangeEnemy;

                gameEvent.Raise();

                for (int i = 0; i < randomWave.numberMeleeEnemy; i++)
                {
                    randomEnemy = Random.Range(0, 2);
                    randomSpawnPoint = Random.Range(0, 6);

                    if (randomEnemy == 0)
                    {
                        enemyMelee = Instantiate(randomWave.MeleeEnemy);
                    }
                    else
                    {
                        enemyMelee = Instantiate(randomWave.MeleeEnemy2);

                    }
                    
                        enemyMelee.gameObject.transform.position = SpawnPoints[randomSpawnPoint].position;
                        
                    }

                for (int j = 0; j < randomWave.numberRangeEnemy; j++)
                {
                    randomEnemy = Random.Range(0, 2);
                    randomSpawnPoint2 = Random.Range(0, 6);
                    if (randomEnemy == 0)
                    {
                        enemyRange = Instantiate(randomWave.RangeEnemy);
                    }
                    else
                    {
                        enemyRange = Instantiate(randomWave.RangeEnemy2);

                    }

                    enemyRange.gameObject.transform.position = SpawnPoints[randomSpawnPoint2].position;

                }

                break;
        }
    }

    public void runWave()
    {

        Waves(GameManager.Instance.GetSetWave);
    }
   
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace m08m17
{
    public class ObservadorGameEventParametre : MonoBehaviour
    {
        private SpriteRenderer m_Sprite;

        private void Awake()
        {
            m_Sprite = GetComponent<SpriteRenderer>();
        }

        public void EventObservat(Vector3 parametre)
        {
            Debug.Log(string.Format("S'ha produit un GameEvent amb valor {1} i s�c {0}",
                name,
                parametre));
            transform.position = new Vector3(Random.Range(-9f, 9f), Random.Range(-5f, 5f), 0);
            m_Sprite.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        }
    }
}

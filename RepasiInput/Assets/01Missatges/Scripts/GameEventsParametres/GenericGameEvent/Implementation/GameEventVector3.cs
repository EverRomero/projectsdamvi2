using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace m08m17
{
    [CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/GameEvent - Vector3")]
    public class GameEventVector3 : GameEvent<Vector3> { }
}

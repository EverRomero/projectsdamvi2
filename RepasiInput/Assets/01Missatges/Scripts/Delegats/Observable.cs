using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace m08m17
{
    public class Observable : MonoBehaviour
    {
        public delegate void EventSucceit();
        public event EventSucceit OnSucceit;

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
                OnSucceit?.Invoke(); // ==> if(OnSucceit != null) OnSucceit.Invoke();
        }
    }
}
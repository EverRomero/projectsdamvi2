using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace m08m17
{
    public class ObservableGameEvent : MonoBehaviour
    {
        [SerializeField]
        private GameEvent m_Event;

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
                m_Event.Raise(); // ==> if(OnSucceit != null) OnSucceit.Invoke();
        }
    }
}
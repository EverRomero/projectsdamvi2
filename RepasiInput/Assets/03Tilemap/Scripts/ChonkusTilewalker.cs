using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
namespace m08m17
{
    public class ChonkusTilewalker : MonoBehaviour
    {

        [SerializeField]
        private InputActionAsset m_InputAsset;
        private InputActionAsset m_Input;

        private InputAction m_MovementAction;

        private Rigidbody2D m_Rigidbody;
        private float m_Speed = 3f;

        private int m_WaterLayer = 0;

        // Start is called before the first frame update
        void Start()
        {
            m_Rigidbody = GetComponent<Rigidbody2D>();

            m_WaterLayer = LayerMask.NameToLayer("Water");

            m_Input = Instantiate(m_InputAsset);
            m_MovementAction = m_Input.FindActionMap("Default").FindAction("MovementAction");
            m_Input.FindActionMap("Default").Enable();
        }

        Vector2 m_Movement = Vector2.zero;
        // Update is called once per frame
        void Update()
        {
            m_Movement = m_MovementAction.ReadValue<Vector2>();
        }

        private void FixedUpdate()
        {
            m_Rigidbody.velocity = m_Movement * m_Speed;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.attachedRigidbody.gameObject.layer == m_WaterLayer)
                Debug.Log("MEOWWWW");
        }
    }
}

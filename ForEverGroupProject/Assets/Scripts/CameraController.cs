using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public GameObject currentRoom;

    public void ChangeRoomCamera()
    {
        Debug.Log(currentRoom.name);

        this.transform.position = new Vector3(currentRoom.transform.position.x, currentRoom.transform.position.y, -10);
    }

    private void Update()
    {
        
        if(this.transform.position != new Vector3(currentRoom.transform.position.x, currentRoom.transform.position.y, -10))
        {

            this.transform.position = new Vector3(currentRoom.transform.position.x, currentRoom.transform.position.y, -10);
        }

    }

}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    private PlayerInput playerInput;
    private Vector2 movement;
    private Animator animator;
    private Sprite actualSprite;
    private Rigidbody2D playerRigidbody;

    //[SerializeField] private HitboxInfo hitboxInfo;

    [Space(10)]
    [Header("Stats of player")]
    //[SerializeField] private SOPlayer playerSO;
    private float speed = 5;
    private float maxHP;
    private float actualHP;
    private int attack;
    private int defense;
    private bool iCanDash;
    private Vector2 savedVelocity;


    [Header("Memory variables")]
    [HideInInspector] public bool comboDone;
    [HideInInspector] public bool comboUp;

    private enum SwitchMachineStates { NONE, IDLE, WALK, DASH, ATTACK, RECIVEHIT, DIE };
    private SwitchMachineStates currentState;
    private SwitchMachineStates lastState;

    private enum SwitchAttackStates { NONATTACK, ATTACK1, ATTACK2, ATTACK3 }
    private SwitchAttackStates attackState;

    void Start()
    {

        playerInput = GetComponent<PlayerInput>();
        playerRigidbody = GetComponent<Rigidbody2D>();
        iCanDash = true;

        InitState(SwitchMachineStates.IDLE);
    }


    // Update is called once per frame
    void Update()
    {
        UpdateState();
        movement = playerInput.actions["Move"].ReadValue<Vector2>();
    }

    #region SwitchMachineStates

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == currentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        this.currentState = currentState;
        switch (this.currentState)
        {
            case SwitchMachineStates.IDLE:

                playerRigidbody.velocity = Vector2.zero;

                break;


            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.DASH:

                break;

            case SwitchMachineStates.ATTACK:
                playerRigidbody.velocity = Vector2.zero;

                break;


            case SwitchMachineStates.RECIVEHIT:
                playerRigidbody.velocity = Vector2.zero;

                break;

            case SwitchMachineStates.DIE:

                Destroy(gameObject);

                break;

            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (currentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.DASH:
                break;

            case SwitchMachineStates.ATTACK:
                attackState = SwitchAttackStates.NONATTACK;
                break;

            case SwitchMachineStates.RECIVEHIT:
                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (currentState)
        {
            case SwitchMachineStates.IDLE:

                if (movement != Vector2.zero)
                    ChangeState(SwitchMachineStates.WALK);

                break;

            case SwitchMachineStates.WALK:

                playerRigidbody.velocity = movement * speed;

                if (playerRigidbody.velocity == Vector2.zero)
                    ChangeState(SwitchMachineStates.IDLE);
                if (playerRigidbody.velocity.y > 0 && playerRigidbody.velocity.x == 0)
                    transform.eulerAngles = new Vector3(0, 0, 0);
                if (playerRigidbody.velocity.y < 0 && playerRigidbody.velocity.x == 0)
                    transform.eulerAngles = new Vector3(0, 0, 180);
                if (playerRigidbody.velocity.x < 0 && playerRigidbody.velocity.y == 0)
                    transform.eulerAngles = new Vector3(0, 0, 90);
                if (playerRigidbody.velocity.x > 0 && playerRigidbody.velocity.y == 0)
                    transform.eulerAngles = new Vector3(0, 0, 270);

                lastState = currentState;
                break;

            case SwitchMachineStates.DASH:
                break;

            case SwitchMachineStates.ATTACK:
                break;

            default:
                break;
        }
    }

    public void Attack1Action()
    {
        switch (attackState)
        {
            case SwitchAttackStates.NONATTACK:
                ChangeState(SwitchMachineStates.ATTACK);
                attackState = SwitchAttackStates.ATTACK1;
                break;
            case SwitchAttackStates.ATTACK1:
                attackState = SwitchAttackStates.ATTACK2;
                break;
            case SwitchAttackStates.ATTACK2:
                attackState = SwitchAttackStates.ATTACK3;
                break;
        }
    }

    public void EndAnimation()
    {
        ChangeState(lastState);
    }

    #endregion

    public void Dash()
    {
        ChangeState(SwitchMachineStates.DASH);
        if (iCanDash)
        {
            iCanDash = false;
            savedVelocity = playerRigidbody.velocity;
            playerRigidbody.velocity = playerRigidbody.velocity.normalized * 20;
            StartCoroutine(DashInAction());
        }
    }

    IEnumerator DashInAction()
    {
        yield return new WaitForSeconds(0.2f);
        playerRigidbody.velocity = savedVelocity;
        iCanDash = true;
        ChangeState(lastState);
    }

}